const tokenGenerator = require('./tokenGenerator.js');
const randomColor = require('./randomColors.js');
const getRandomInt = require('./randomInt.js');
const _ = require('lodash');

const init = (width, height, numberOfPlayers) => {

  const arenaData = {};
  arenaData.step = 0;

  const makeArena = (width, height) => {
    const Arena = [];
    for (let i = 0; i < height; i++) {
      Arena.push(Array(width + 1).join(' ').split(''));
    }
    return Arena;
  };

  const generatePlayers = (numberOfPlayers, width, height) => {
    const players = [];
    for (let i = 0; i < numberOfPlayers; i++) {
      let possible_x = getRandomInt(0, width - 1);
      let possible_y = getRandomInt(0, height - 1);
      let checking = true;
      while (checking) {
        let foundCollision = false;
        for (let j = 0; j < players.length; j++) {
          if (players[j].location.x === possible_x && players[j].location.y === possible_y) {
            foundCollision = true;
          }
        }

        if (foundCollision) {
          possible_x = getRandomInt(0, width - 1);
          possible_y = getRandomInt(0, height - 1);
        } else {
          checking = false;
        }
      }
      players.push({
        playerName: (process.env['PLAYER_'+(i + 1)+'_NAME']) ? process.env['PLAYER_'+(i + 1)+'_NAME'] : 'PLAYER_'+ (i + 1),
        color: (process.env['PLAYER_'+(i + 1)+'_COLOR']) ? process.env['PLAYER_'+(i + 1)+'_COLOR'] : randomColor(0.3, 0.95).hex().toString(),
        direction: _.sample(['←', '↑', '→', '↓']), // ['←' '↑' '→' '↓']
        location: {
          x: getRandomInt(0, width - 1),
          y: getRandomInt(0, height - 1),
        },
        token: (process.env['PLAYER_'+(i + 1)+'_TOKEN']) ? process.env['PLAYER_'+(i + 1)+'_TOKEN'] : tokenGenerator.generateToken(),
        steps: 0,
        type: 'player',
        alive: true,
        setWallCount: 0,
      });
    }
    return players;
  };

  const getArenaState = () => {
    let MyNewArenaState = arenaData.arenaFields;
    for (let i = 0; i < arenaData.players.length; i++) {
      const x = arenaData.players[i].location.x;
      const y = arenaData.players[i].location.y;
      if (typeof MyNewArenaState[y] !== 'undefined'){
        if (typeof MyNewArenaState[y][x] !== 'undefined'){
          MyNewArenaState[y][x] = {
            ...arenaData.players[i],
          };
        }
      }
    }
    return MyNewArenaState;
  };

  const rotatePlayerLeft = (i) => {
    if (arenaData.players[i].alive === false) { return; }
    if (arenaData.players[i].direction === '↑') {
      arenaData.players[i].direction = '←';
      return;
    }
    if (arenaData.players[i].direction === '←') {
      arenaData.players[i].direction = '↓';
      return;
    }
    if (arenaData.players[i].direction === '↓') {
      arenaData.players[i].direction = '→';
      return;
    }
    if (arenaData.players[i].direction === '→') {
      arenaData.players[i].direction = '↑';
      return;
    }
  };

  const rotatePlayerRight = (i) => {
    if (arenaData.players[i].alive === false) { return; }
    if (arenaData.players[i].direction === '↑') {
      arenaData.players[i].direction = '→';
      return;
    }
    if (arenaData.players[i].direction === '→') {
      arenaData.players[i].direction = '↓';
      return;
    }
    if (arenaData.players[i].direction === '↓') {
      arenaData.players[i].direction = '←';
      return;
    }
    if (arenaData.players[i].direction === '←') {
      arenaData.players[i].direction = '↑';
      return;
    }
  };

  const nextTick = () => {
    arenaData.step += 1;
    // Moving the players based on direction
    for (let i = 0; i < arenaData.players.length; i++) {
      if (arenaData.players[i].alive) {

        const oldLocation = { ...arenaData.players[i].location };
        let newLocation  = { ...arenaData.players[i].location };
        if (arenaData.players[i].direction === '↑') {
          newLocation.y -= 1;
        }
        if (arenaData.players[i].direction === '↓') {
          newLocation.y += 1;
        }
        if (arenaData.players[i].direction === '←') {
          newLocation.x -= 1;
        }
        if (arenaData.players[i].direction === '→') {
          newLocation.x += 1;
        }

        // New position is valid position
        if (typeof arenaData.arenaFields[newLocation.y] !== 'undefined'){
          if (typeof arenaData.arenaFields[newLocation.y][newLocation.x] !== 'undefined'){

            // Check collision
            if (typeof arenaData.arenaFields[newLocation.y][newLocation.x] === 'object') {
              arenaData.players[i].alive = false;
              continue;
            }

            if (arenaData.players[i].setWallCount === 0 || arenaData.players[i].setWallCount === 1) {
              arenaData.arenaFields[oldLocation.y][oldLocation.x] = {
                playerName: arenaData.players[i].playerName,
                color: arenaData.players[i].color,
                type: 'wall',
              };
            } else {
              arenaData.arenaFields[oldLocation.y][oldLocation.x] = ' ';
            }

            arenaData.players[i].location = newLocation;
            arenaData.players[i].setWallCount++;
            if (arenaData.players[i].setWallCount === 4) {
              arenaData.players[i].setWallCount = 0;
            }

            arenaData.players[i].steps += 1;

            continue;
          }
        }
        arenaData.players[i].alive = false;

      }
    }
  };

  const getStepCount = () => {
    return arenaData.step;
  }

  arenaData.arenaFields = makeArena(width, height);
  arenaData.players = generatePlayers(numberOfPlayers, width, height);

  return {
    ...arenaData,
    getArenaState,
    nextTick,
    rotatePlayerLeft,
    rotatePlayerRight,
    getStepCount,
  };
};

module.exports = {
  init,
};