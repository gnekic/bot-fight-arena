const crypto = require('crypto');

const randomString = (_n, _possible) => {
  let text = '';
  const possible = _possible || 'abcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < _n; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

const getRandomHash = () => {
  const content = randomString(64).toUpperCase();
  const key = randomString(64).toUpperCase();
  return crypto.createHmac('sha256', key).update(content).digest('hex');
};

const generateToken = () => {
  return randomString(16).toUpperCase();
};

module.exports = {
  generateToken,
  getRandomHash,
};