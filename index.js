/*
       _  __                       _____        __   ___   ___  __  ___    
      | |/ /                      |_   _|      / _| |__ \ / _ \/_ |/ _ \   
      | ' / __ _ _ __ ___  _ __     | |  _ __ | |_     ) | | | || | (_) |  
      |  < / _` | '_ ` _ \| '_ \    | | | '_ \|  _|   / /| | | || |> _ <   
      | . \ (_| | | | | | | |_) |  _| |_| | | | |    / /_| |_| || | (_) |  
      |_|\_\__,_|_| |_| |_| .__/  |_____|_| |_|_|   |____|\___/ |_|\___/   
  ____        _   ______ _| |    _     _                                   
 |  _ \      | | |  ____(_)_|   | |   | |       /\                         
 | |_) | ___ | |_| |__   _  __ _| |__ | |_     /  \   _ __ ___ _ __   __ _ 
 |  _ < / _ \| __|  __| | |/ _` | '_ \| __|   / /\ \ | '__/ _ \ '_ \ / _` |
 | |_) | (_) | |_| |    | | (_| | | | | |_   / ____ \| | |  __/ | | | (_| |
 |____/ \___/ \__|_|    |_|\__, |_| |_|\__| /_/    \_\_|  \___|_| |_|\__,_|
                            __/ |                                          
                           |___/                                           
*/

// ################################################################################

	// Load environment variable from .env file if not supplied
	const dotenv = require('dotenv').config();

	// Load all the stuff we'll need
	const _ = require('lodash');
	const express = require('express');
	const cors = require('cors');
	const fallback = require('express-history-api-fallback');

	const app = express();
	const http = require('http').Server(app);
	const io = require('socket.io')(http);

	// Load all the things for pretty CLI
	const figlet = require('figlet');
	require('colors');

// ################################################################################

	// Clear the console
	console.log('\x1Bc' + Array(80 + 1).join('#').yellow);

	// Write something fun to console
	// console.log(Array(80 + 1).join('#').yellow);
	console.log(Array(80 + 1).join(' ').yellow.bgBlue);
	console.log(figlet.textSync(' Bot Fight Arena  ', {
		font: 'Big',
		horizontalLayout: 'default',
		verticalLayout: 'default',
	}).white.bgBlue);
	console.log(Array(80 + 1).join('#').yellow);

// ################################################################################

	const arenaMaker = require('./lib/arena.js');
	const prettyPrint = require('./lib/prettyPrint.js');

	// Terrain Bot Fight Arenas - Team Fight
	let defaultArena = arenaMaker.init(36, 36, 4, io);
	let anotherArena = arenaMaker.init(10, 10, 1, io);

	// console.log(defaultArena.players);
	
	// console.log(prettyPrint(defaultArena.players));
	// console.log(prettyPrint(anotherArena.players));

	function findIndexByToken(token) {
		for (let i = 0; i < defaultArena.players.length; i++) {
			if (defaultArena.players[i].token === token) {
				return i;
			}
		}
		return -1;
	}

// ################################################################################

	// Set the port
	const SERVER_PORT = process.env.SERVER_PORT || 8080;
	// Set endpoint
	const APIEndpoint = 'endpoint';

	app.use(express.static(`${__dirname}/public`));

	app.use(cors());

	app.use(fallback('index.html', { root: `${__dirname}/public` }));

// ################################################################################

	const socketIoData = {};
	socketIoData.connectionCount = 0;
	socketIoData.playersAuthenticated = 0;

	io.on('connection', (socket) => {
		// New client connected do some stuff!
		console.log('[socket.io] New client connected!');
		const connectionData = {};
		socketIoData.connectionCount += 1;
		connectionData.playerIndex = -1;

		// Helper functions
		function message(message) {
			console.log(('[socket.io] System message: ' + message).bgYellow);
			socket.emit('system message', message);
		}
		function error(error, code) {
			console.log(('[socket.io] ERROR: ' + error).bgRed);
			socket.emit('error message', error, code);
			throw new Error(JSON.stringify({ code: code, message: error }));
		}

		// Game functions
		const startGameFunction = (token) => {
			try {
				// Check for type
				if (typeof token !== 'string') { error('Authentication token is not a string.'); }
				// Check for format
				if (!token.match(/^[a-f\d]{64}$/i)) { error('Authentication token is not a valid format.'); }

				// Do stuff
				console.log('[socket.io] Start!');

				if (token === process.env.ADMIN_TOKEN) {
					gameIsWorking = true;
					message('Start successful.');
				}

			} catch(error) {
				// console.log('[CRITICAL ERROR]', error);
			}
		};

		const restartGameFunction = (token) => {
			try {
				// Check for type
				if (typeof token !== 'string') { error('Authentication token is not a string.'); }
				// Check for format
				if (!token.match(/^[a-f\d]{64}$/i)) { error('Authentication token is not a valid format.'); }

				// Do stuff
				console.log('[socket.io] Restart!');

				if (token === process.env.ADMIN_TOKEN) {
					gameIsWorking = false;
					gameHasStarted = true;
					defaultArena = arenaMaker.init(36, 36, 4, io);
					message('Restart successful.');
				}

			} catch(error) {
				// console.log('[CRITICAL ERROR]', error);
			}
		};

		const authenticateFunction = (token) => {
			try {
				// Check for type
				if (typeof token !== 'string') { error('Authentication token is not a string.'); }
				
				// Do stuff
				console.log('[socket.io] Player auth: ' + token);

				connectionData.playerIndex = findIndexByToken(token);
				if (connectionData.playerIndex !== -1) {
					socket.emit('login');
					socket.join('player_'+connectionData.playerIndex);
					socketIoData.playersAuthenticated += 1;
					console.log(('Total authenticated users: ' + socketIoData.playersAuthenticated).bgBlue);
				}

			} catch(error) {
				// console.log('[CRITICAL ERROR]', error);
			}
		};

		const botGoLeftFunction = () => {
			try {
				const playerIndex = _.get(connectionData, 'playerIndex', -1);
				
				if (playerIndex === -1) {
					return;
				}

				if (!(defaultArena.players[playerIndex].alive)) { return; }
				
				// Do stuff
				console.log('[socket.io] Player: '+playerIndex+' going left');
				defaultArena.rotatePlayerLeft(playerIndex);
				socket.emit('command accepted', 'left');
			} catch(error) {
				// console.log('[CRITICAL ERROR]', error);
			}
		};

		const botGoRightFunction = () => {
			try {
				const playerIndex = _.get(connectionData, 'playerIndex', -1);
				
				if (playerIndex === -1) {
					return;
				}

				if (!(defaultArena.players[playerIndex].alive)) { return; }

				// Do stuff
				console.log('[socket.io] Player: '+playerIndex+' going right');
				defaultArena.rotatePlayerLeft(playerIndex);
				socket.emit('command accepted', 'right');
			} catch(error) {
				// console.log('[CRITICAL ERROR]', error);
			}
		};

		const clientDisconnected = () => {
			console.log('[socket.io] Client disconnected');
			socketIoData.connectionCount -= 1;
			if (socketIoData.connectionCount < 0) {
				socketIoData.connectionCount = 0;
				console.log('[socket.io] Connection count is under 0 !!!'.bgRed);
			}
			const playerIndex = _.get(connectionData, 'playerIndex', -1);
			if (playerIndex !== -1) {
				socketIoData.playersAuthenticated -= 1;
			}
		};

		// Listen to events from clients
		socket.on('start', startGameFunction);
		socket.on('restart', restartGameFunction);
		socket.on('auth', authenticateFunction);
		socket.on('left', botGoLeftFunction);
		socket.on('right', botGoRightFunction);
		socket.on('disconnect', clientDisconnected);

	});

// ################################################################################

	let gameIsWorking = false;
	let gameHasStarted = true;

	const serverTickMs = 1000;
	const serverTick = setInterval(() => {
		const globalSocketData = {};
		globalSocketData.connectionCount = socketIoData.connectionCount;
		io.emit('tick', globalSocketData);

		if (gameIsWorking === true) {
			const safeOutputArenaState = defaultArena.getArenaState().slice();
			for (let i = 0; i < safeOutputArenaState.length; i++) {
				for (let j = 0; j < safeOutputArenaState[i].length; j++) {
					if (typeof safeOutputArenaState[i][j] === 'object') {
						if (safeOutputArenaState[i][j].type === 'player') {
							delete safeOutputArenaState[i][j].token;
						}
					}
				}
			}
			io.emit('arena global', safeOutputArenaState, undefined, defaultArena.getStepCount());
			for (let i = 0; i < defaultArena.players.length; i++) {
				io.to('player_'+i).emit('arena', safeOutputArenaState, defaultArena.players[i], defaultArena.getStepCount());
			}
			

			const safeOutputPlayers = {
				...defaultArena.players,
			};
			delete safeOutputPlayers.token;
			io.emit('players', defaultArena.players);
			
			if (gameHasStarted) {
				gameHasStarted = false;
			} else {
				defaultArena.nextTick();
			}
		}
	}, serverTickMs);

// ################################################################################

	http.listen(SERVER_PORT, () => {

		console.log(`Running a ${'HTTP'.green} server at ${(`localhost:${SERVER_PORT}/${APIEndpoint}`).green}\n` +
		'\n\n' +
		' Project: ' + 'Bot Fight!\n'.green +
		'  Author: ' + 'Gordan Nekic\n'.green);

		console.log(Array(80 + 1).join('#').yellow);

	});

// ################################################################################

	if (process.env.NODE_ENV === 'development-debug' || process.env.NODE_ENV === 'development') {
		console.log('                   WARNING!!! DEVELOPMENT/DEBUG MODE ENABLED!                   '.bgRed);
		console.log(Array(80 + 1).join('#').yellow);
	}
